using Квест;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
int res = 2;
Console.WriteLine("Как звать?");
string name = Console.ReadLine();
Person prsn = new Person(name);
Console.WriteLine($"Привет, {name}, ты вышел из дома. Выбери свой путь.");

void select()
{
    Console.WriteLine("1 - Пойти в шкилу 2 - Пойти за шавой");
    int x = Convert.ToInt32(Console.ReadLine());
    if (x == 1)
    {
        Console.Clear();
        Console.WriteLine("Поехали");
        Console.WriteLine();
        Console.Clear();
        lvl1();
    }
    else if(x == 2)
    {
        Console.WriteLine("ТЫ СПИЛСЯ!!!");
        Console.WriteLine("GAME OVER");
    }
    else
    {
        while (true)
        {
            if (x != 1 || x != 2)
            {
                Console.WriteLine("Введите число из правильного диапазона");
                select();
            }
        }
    }
}

void lvl1()
{
    Console.WriteLine("Сложный путь выбрал. Выбирай как поступить дальше.");
    Console.WriteLine("1 - Отсидеть срок на уроке");
    Console.WriteLine("2 - Сбежать за шавой");
    Console.WriteLine("Выбирай");
    int i = Convert.ToInt32(Console.ReadLine());
    if (i == 1)
    {
        Console.Clear();
        Console.WriteLine("Отматав срок длиною в 11 лет, тебе открывается новая локация: ВУЗ.");
        Console.WriteLine("Ну или можешь пойти в шарагу...");
        lvl2();
    }
    else if (i == 2)
    {
        Console.Clear();
        Console.WriteLine("Подходя к шавермошной ты замечаешь что осталась одна шаверма.");
        Console.WriteLine("Время эпической битвы за шаву.");
        Console.ReadLine();
        Console.Clear();
        fight();

    }
    else
    {
        Console.Clear();
        while (true)
        {
            if (i != 1 || i != 2)
            {
                Console.WriteLine("введите число из правильного диапазона");
                lvl1();
            }
        }
    }
}

void lvl2()
{
    Console.WriteLine("1 - ВУЗ");
    Console.WriteLine("2 - шарага");
    int i = Convert.ToInt32(Console.ReadLine());
    if (i == 1)
    {
        Console.WriteLine("ВУЗ ВУЗ'ом, а курить хочется...");
        Console.WriteLine("1 - Стрельнуть сигу в курилке");
        Console.WriteLine("2 - Сидеть на парах");
        int u = Convert.ToInt32(Console.ReadLine());
        if (u == 1)
        {
            prsn.hp = 90;
            prsn.dmg = 25;
            Console.WriteLine("Не следя за манерами, ты быканул на главаря курилки.");
            Console.WriteLine("Сражайся до последнего...");
            Console.ReadLine();
            Console.Clear();
           
            fight2();
        }
        else if (u == 2)
        {
            Console.Clear();
            Console.WriteLine("Поздравляем!!!!!");
            Console.WriteLine("Отсидев все пары на протяжении 4-ёх лет");
            Console.WriteLine("Ты можешь работать за 225000 руб. в месяц");
            Console.WriteLine("В ТРК Маяк, в Бургер Кинге");
            select();
        }
    }
    
    
    else if (i == 2)
    {
        Console.WriteLine("Выбрал шарагу значит...");
        Console.WriteLine("Сразись за место в шараге и получе аттестат алкаша!");
        Console.ReadLine();
        Console.Clear();
      
        fight3();
    }      
}
Random rnd = new Random();
void fight()
{
    Enemy aboba = new Enemy();
    prsn.hp -= 20;
    Console.WriteLine($"На тебя напал Пётр Палыч и отнял 20 xп");
    Console.WriteLine("Для того, чтобы ударить противника нажмите нажмите enter");
    while (prsn.hp > 0 || aboba.hp > 0)
    {
        Console.ReadLine();
        aboba.hp -= prsn.dmg;
        Console.WriteLine("Твой удар " + prsn.dmg);
        if (aboba.hp <= 0)
        {
            res = 1;
            Console.WriteLine("Здоровье противника - 0");
            break;
        }
        Console.WriteLine("Здоровье противника " + aboba.hp);
        Console.WriteLine("---------------");
        Console.WriteLine("Удар противника " + aboba.damage());
        prsn.hp -= aboba.damage();
        if (prsn.hp <= 0)
        {
            res = 0;
            Console.WriteLine("Твоё здоровьё - 0");
            break;
        }
        Console.WriteLine("Твоё здоровье " + prsn.hp);
        Console.WriteLine("---------------");
    }
    if (res == 1)
    {
        Console.WriteLine("Ты выиграл");
        Console.ForegroundColor = ConsoleColor.Green;
        Console.WriteLine("Победа");
        Console.ResetColor();
    }
    else if (res == 0)
    {
        Console.WriteLine("Ты проиграли");
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine("Поражение");
        Console.ResetColor();
    }
    Console.ReadLine();
    select();
    
}

void fight2()
{
    Enemy aboba = new Enemy();
    prsn.hp -= 5;
    Console.WriteLine($"На тебя напал глава курилки, Артёмыч Иванько и отнял 5 xп");
    Console.WriteLine("Для того, чтобы ударить противника нажмите нажмите enter");
    while (prsn.hp > 0 || aboba.hp > 0)
    {
        Console.ReadLine();
        aboba.hp -= prsn.dmg;
        Console.WriteLine("Твой удар " + prsn.dmg);
        if (aboba.hp <= 0)
        {
            res = 1;
            Console.WriteLine("Здоровье противника - 0");
            break;
        }
        Console.WriteLine("Здоровье противника " + aboba.hp);
        Console.WriteLine("---------------");
        Console.WriteLine("Удар противника " + aboba.damage());
        prsn.hp -= aboba.damage();
        if (prsn.hp <= 0)
        {
            res = 0;
            Console.WriteLine("Твоё здоровьё - 0");
            break;
        }
        Console.WriteLine("Твоё здоровье " + prsn.hp);
        Console.WriteLine("---------------");
    }
    if (res == 1)
    {
        Console.WriteLine("Сразив босса курилки, ты стал активно употреблять алкоголь...");
        Console.WriteLine("Ты выиграл, но какой ценой?");
        Console.ForegroundColor = ConsoleColor.Green;
        Console.WriteLine("Победа?");
        Console.ResetColor();
    }
    else if (res == 0)
    {
        Console.WriteLine("Ты проиграл, босс курилки потушил о тебя бычёк...");
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine("Поражение");
        Console.ResetColor();
    }
    Console.ReadLine();
    select();

}

void fight3()
{
    Enemy aboba = new Enemy();
    prsn.hp -= 15;
    Console.WriteLine($"На тебя напал ректор шараги, Темычко Кукушкин и отнял 15 xп");
    Console.WriteLine("Для того, чтобы ударить противника нажмите нажмите enter");
    while (prsn.hp > 0 || aboba.hp > 0)
    {
        Console.ReadLine();
        aboba.hp -= prsn.dmg;
        Console.WriteLine("Твой удар " + prsn.dmg);
        if (aboba.hp <= 0)
        {
            res = 1;
            Console.WriteLine("Здоровье противника - 0");
            break;
        }
        Console.WriteLine("Здоровье противника " + aboba.hp);
        Console.WriteLine("---------------");
        Console.WriteLine("Удар противника " + aboba.damage());
        prsn.hp -= aboba.damage();
        if (prsn.hp <= 0)
        {
            res = 0;
            Console.WriteLine("Твоё здоровьё - 0");
            break;
        }
        Console.WriteLine("Твоё здоровье " + prsn.hp);
        Console.WriteLine("---------------");
    }
    if (res == 1)
    {
        Console.WriteLine("У тебя получилось!!!");
        Console.WriteLine("Ты овладел всеми навыками алкаша и у тебя есть диплом, подтверждающий это!");
        Console.ForegroundColor = ConsoleColor.Green;
        Console.WriteLine("Концовка АЛКАШ пройдена");
        Console.ResetColor();
    }
    else if (res == 0)
    {
        Console.WriteLine("Ты проиграл, тебе никогда не стать алкашом...");
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine("Поражение");
        Console.ResetColor();
    }
    Console.ReadLine();
    select();

}

select();
